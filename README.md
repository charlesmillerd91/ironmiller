**Ironmiller**

<i>O Ironmiller é um robô melee, desenvolvido para combates em diversas situações. Principalmente em um ringue cheio de adversários. Ele possui um sistema que escaneia a distancia dos inimigos e escolhe a força dos disparos para realizar um tiro eficaz e, quando com carga suficiente, dispara um tiro com força total. Diminuindo qualquer chance de quem estiver em sua mira. Todos os seus movimentos foram baseados na experiência que obteve durante seus testes. Tornando-o assim um robô jovem, porém com um bom desempenho no campo de batalha.</i>

**Aprendizado**

<i>Apesar dos comandos simples, foi muito bom para desenvolver o raciocínio lógico e explorar minha criatividade para abstrair o que eu idealizei para a solução deste desafio. Pretendo continuar aprendendo cada vez mais funções e combinações do robocode para estimular cada vez mais a minha lógica em programação.</i>
